import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'viewer-app';
  checkModelPage$: Observable<boolean>;

  constructor(public auth: AuthService, public router: Router, public route: ActivatedRoute) {
    this.checkModelPage$ = route.queryParams.pipe(map( p => {
      if (p["model"]) {
        return true;
      }
      else {
        return false;
      }
    }));
  }

  logoutOfTheAccount() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
