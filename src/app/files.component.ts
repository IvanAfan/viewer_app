import { Component, OnInit, Inject } from '@angular/core';
import { FilesService, FileItem } from './files.service';
import { CatalogService } from './catalog.service';
import { Catalog } from './catalog.service';
import { Observable, combineLatest, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map, concatMap, filter, catchError, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DelDialogComponent } from './del-dialog/del-dialog.component';

// export interface DialogData
// {
//   name: string;
//   id: number;
// }

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  addFOLDER = false;
  addCATALOG = false;
  catalogs$: Observable<Catalog[]>;
  files$: Observable<FileItem>;
  info$: Observable<{ catalogs: Catalog[], folder: FileItem, selectedId: number }>;

  constructor(private files: FilesService, private catalogs: CatalogService,
    private route: ActivatedRoute, private router: Router,
    private auth: AuthService, private dialog: MatDialog) {
    //  this.catalogs$ = catalogs.getCatalogs();
    //  this.files$ = this.route.queryParams.pipe(concatMap(p => this.files.getFile(p["ID"], p["children"]=true)));
    this.updateInfo();
  }

  private updateInfo() {
    // this.info$ = this.auth.isAuthenticated.pipe(
    //   filter(v => v),
    //   concatMap(_ => combineLatest(this.catalogs.getCatalogs(), this.route.queryParams,
    //     (catalogs, params) => ({ catalogs, params }))),
    //   concatMap(result => {
    //     let catalog = result.catalogs.find(c => c.id.toString() === result.params["ID"]);
    //     if (catalog) {
    //       return this.files.getFile(catalog.modelFolderId, true)
    //         .pipe(map(folder => ({ catalogs: result.catalogs, folder })))
    //     } else if (result.catalogs.length > 0) {
    //       return this.files.getFile(result.catalogs[0].modelFolderId, true)
    //         .pipe(map(folder => ({ catalogs: result.catalogs, folder })))
    //     }
    //     else return of({ catalogs: undefined as Catalog[], folder: undefined as FileItem })
    //   }));

    this.info$ = combineLatest(this.catalogs.getCatalogs(), this.route.queryParams,
          (catalogs, params) => ({ catalogs, params })).pipe(
        concatMap(result => {
          let catalog = result.catalogs.find(c => c.id.toString() === result.params["id"]);
          if (catalog) {
            return this.files.getFile(catalog.modelFolderId, true)
              .pipe(map(folder => ({ catalogs: result.catalogs, folder, selectedId: catalog.id })))
          } else if (result.catalogs.length > 0) {
            // this.openCatalog(result.catalogs[0].id);
            return this.files.getFile(result.catalogs[0].modelFolderId, true)
              .pipe(map(folder => ({ catalogs: result.catalogs, folder, selectedId: result.catalogs[0].id})))
          }
          return of({ catalogs: undefined as Catalog[], folder: undefined as FileItem, selectedId: undefined as number  });
        }));
  }


  openCatalog(id: number) {
    this.router.navigate([], { queryParams: { id } });

  }

  openModel(id: number) {
    this.router.navigate(['/model', id], { queryParams: { model: true }});
  }



  addNewCatalog(name: string) {
    this.catalogs.addCatalog(name).subscribe(c => {this.openCatalog(c.id); this.updateInfo()});
    //  this.info$.pipe(map(info => {
    //    let c = info.catalogs.find(c => c.name===name);
    //    return c.id;
    //   })).subscribe(id => this.openCatalog(id));
  }

  removeCatalog(event: MouseEvent, id: number) {
    event.stopPropagation();
    this.catalogs.removeCatalog(id).subscribe(_ => this.updateInfo());
  }

  delFile(event: MouseEvent, file: FileItem) {
    event.stopPropagation();
    this.files.removeFile(file).subscribe(_ => this.updateInfo());
  }

  addNewFolder(id: number, name: string) {
    // this.route.queryParams.pipe(
    //   concatMap(query => this.catalogs.getCatalog(query["ID"])),
    //   concatMap(catalog => this.files.addFolder(catalog.modelFolderId, name))
    // ).subscribe(_=> this.updateInfo());
    this.files.addFolder(id, name).subscribe(_ => this.updateInfo());
  }

  uploadFile(event: Event, folderId: number) {
    let input = event.target as HTMLInputElement;
    if (input.files.length === 1) {
      let file = input.files.item(0);
      this.files.uploadFile(folderId, file).subscribe(_ => this.updateInfo());
    }
  }

  openDialogDelFile(event: MouseEvent, file: FileItem) {
    event.stopPropagation();
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = "file " + file.name;

    const dialogRef = this.dialog.open(DelDialogComponent, dialogConfig);

    dialogRef.afterClosed().pipe(
      filter(v => v),
      concatMap(_ => this.files.removeFile(file))
    ).subscribe(_ => this.updateInfo());
  }

  openDialogDelCatalog(event: MouseEvent, catalog: Catalog) {
    event.stopPropagation();
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = "catalog " + catalog.name;

    const dialogRef = this.dialog.open(DelDialogComponent, dialogConfig);

    dialogRef.afterClosed().pipe(
      filter(v => v),
      concatMap(_ => this.catalogs.removeCatalog(catalog.id))
    ).subscribe(_ => this.updateInfo());
  }


  dropHandler(ev, folderId: number) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (var i = 0; i < ev.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (ev.dataTransfer.items[i].kind === 'file') {
          let file = ev.dataTransfer.items[i].getAsFile();
          this.files.uploadFile(folderId, file).subscribe(_ => this.updateInfo());
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (var i = 0; i < ev.dataTransfer.files.length; i++) {
        this.files.uploadFile(folderId, ev.dataTransfer.files[i]).subscribe(_ => this.updateInfo());
        //console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
      }
    }
    // Pass event to removeDragData for cleanup
    this.removeDragData(ev)
  }

  removeDragData(ev) {
    //console.log('Removing drag data')
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to remove the drag data
      ev.dataTransfer.items.clear();
    } else {
      // Use DataTransfer interface to remove the drag data
      ev.dataTransfer.clearData();
    }
  }

  dragOverHandler(ev) {
    //console.log('File(s) in drop zone');
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  }

  ngOnInit() {
  }

}
