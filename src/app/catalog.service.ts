import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DomSanitizer } from "@angular/platform-browser";
import { map, concatMap } from 'rxjs/operators';

export enum CatalogType {
  Model = 0,
  Material = 1,
  Planner = 2,
  ModelAndMaterial = 3
}

export interface Catalog {
  id: number;
  name: string;
  description: string;
  preview?: string;
  parentCatalogId: number;
  modelFolderId: number;
  materialGroupId: number;
  shared: boolean;
  ownerId: number;
  readOnly: boolean;
  type: CatalogType;
}

export interface CatalogSyncConfig {
  catalogUrl: string;
  username: string;
  password: string;
  destFolderId?: number;
  updateExistingModels: boolean;
  removeMissingModels: boolean;
}

export enum MaterialUnit {
  None = 0,
  Units = 1,
  Meters = 2,
  SquareMeters = 3,
  CubicMeters = 4
}

export enum MaterialType {
  Material = 0,
  Group = 1
}

export interface CatalogMaterial {
  id?: number;
  type: MaterialType;
  groupId?: number;
  catalogId: number;
  name: string;
  texture: string;
  bumpTexture: string;
  sizex: number;
  sizey: number;
  offsetx: number;
  offsety: number;
  angle: number;
  transparency: number;
  reflection: number;
  ambient: number;
  specular: number;
  shininess: number;
  price: number;
  unit: MaterialUnit;
}

export interface CatalogGroup {
  id?: number;
  type: MaterialType.Group;
  groupId?: number;
  catalogId: number;
  name: string;
  texture?: string;
  readOnly: boolean;
  materials: CatalogMaterial[];
}

export interface UpdateMaterialResponse {
  changedModels: number;
}

export function createMaterial(
  name: string,
  catalog?: number
): CatalogMaterial {
  return {
    id: undefined,
    type: MaterialType.Group,
    catalogId: catalog,
    name,
    texture: undefined,
    bumpTexture: undefined,
    sizex: 100,
    sizey: 100,
    offsetx: 0,
    offsety: 0,
    angle: 0,
    transparency: 0,
    reflection: 0,
    ambient: 0,
    specular: 0.3,
    shininess: 0.4,
    price: 0,
    unit: MaterialUnit.None
  };
}

export function copyMaterial(src: CatalogMaterial) {
  let result = createMaterial('');
  for (let prop in result) {
    result[prop] = src[prop];
  }
  return result;
}

export interface CatalogProperty {
  id: number;
  catalogId: number;
  name: string;
  data: string;
}

@Injectable({
  providedIn: 'root',
})
export class CatalogService {
  constructor(private http: HttpClient) {}

  public getCatalogs(): Observable<Catalog[]> {
    return this.http.get<Catalog[]>(`/api/catalogs/`).pipe(map(list => {
      list.sort((a, b) => {
        if ( !isNaN(Number(a.name)) && !isNaN(Number(b.name)))
        {
         return (Number(a.name) - Number(b.name));
        }
        else if ( isNaN(Number(a.name)) && !isNaN(Number(b.name)))
        {
          return 1;
        }
        else if ( !isNaN(Number(a.name)) && isNaN(Number(b.name)))
        {
          return -1;
        }
        else
        {
        return a.name.localeCompare(b.name);
        }
      })
      return list;
    }));
  }

  public getSharedCatalogs(): Observable<Catalog[]> {
    return this.http.get<Catalog[]>(`/api/catalogs/shared`);
  }

  public getCatalog(id: number): Observable<Catalog> {
    return this.http.get<Catalog>(`/api/catalogs/${id}`);
  }

  public getNestedCatalogs(parentCatalogId: number): Observable<Catalog[]> {
    return this.http.get<Catalog[]>(`/api/catalogs/${parentCatalogId}/nested`);
  }

  public addCatalog(name: string, parentCatalogId?: number): Observable<Catalog> {
    return this.http.post<Catalog>(`/api/catalogs`, { name: name, parentCatalogId });
  }

  public syncCatalog(id: number, config: CatalogSyncConfig) {
    return this.http.post<{modelCount, materialCount, textureCount}>(`/api/catalogs/${id}/sync`, config);
  }

  public removeCatalog(id: number) {
    return this.http.delete(`/api/catalogs/${id}`);
  }

  public shareCatalog(id: number, shared: boolean) {
    return this.http.post(`/api/catalogs/${id}`, { shared });
  }

  public renameCatalog(id: number, name: string, description?: string) {
    return this.http.post(`/api/catalogs/${id}`, { name, description });
  }

  public updateType(id: number, type: number) {
    return this.http.post(`/api/catalogs/${id}`, { type });
  }

  public updateThumbnail(id: number, thumbnail: File) {
    let data = new FormData();
    data.append('file', thumbnail, thumbnail.name);
    return this.http.post<Catalog>(`./api/catalogs/${id}/preview`, data);
  }

  public removeThumbnail(id: number) {
    return this.http.delete(`/api/catalogs/${id}/preview`);
  }

  public getGroup(catalogId, groupId: number): Observable<CatalogGroup> {
    return this.http.get<CatalogGroup>(
      `/api/catalogs/${catalogId}/materialgroup/${groupId}`
    ).pipe(map(g => {
      let compare = (a, b) => a.name.localeCompare(b.name);
      g.materials.sort(compare);
      return g;
    }));
  }

  public getMaterials(catalogId: number): Observable<CatalogMaterial[]> {
    return this.http.get<CatalogMaterial[]>(
      `/api/catalogs/${catalogId}/materials`
    );
  }

  public addMaterial(
    catalogId: number,
    name: string,
    groupId?: number
  ): Observable<CatalogMaterial> {
    return this.http.post<
      CatalogMaterial
    >(`/api/catalogs/${catalogId}/materials`, { name, groupId });
  }

  public addGroup(
    catalogId: number,
    name: string,
    groupId?: number
  ): Observable<CatalogGroup> {
    let data = { name, groupId, type: MaterialType.Group };
    return this.http.post<CatalogGroup>(`/api/catalogs/${catalogId}/materials`, data);
  }

  public updateMaterial(material: CatalogMaterial | CatalogGroup, updateModel?: number): Observable<UpdateMaterialResponse> {
    if (!material.id) {
      return this.addMaterial(material.catalogId, material.name).pipe(concatMap(mat =>
        this.updateMaterial(mat)
      ));
    }
    let params = typeof updateModel === 'number' ? { model: updateModel.toString()} : undefined;
    return this.http.post<UpdateMaterialResponse>(
      `/api/catalogs/${material.catalogId}/materials/${material.id}`,
      material, {params}
    );
  }

  public findMaterial(
    catalogId: number,
    name: string
  ): Observable<CatalogMaterial> {
    return this.http.get<CatalogMaterial>(
      `/api/catalogs/${catalogId}/findmaterial/${name}`
    );
  }

  public findMaterials(pointers: string[]): Observable<CatalogMaterial[]> {
    return this.http.post<CatalogMaterial[]>(
      `/api/catalogs/materials`,
      pointers
    );
  }

  public findModelMaterials(catalogId: number, modelId: number): Observable<CatalogMaterial[]> {
    let url = `/api/catalogs/${catalogId}/model/${modelId}/materials`;
    return this.http.get<CatalogMaterial[]>(url);
  }

  public removeMaterial(m: CatalogMaterial | CatalogGroup) {
    return this.http.delete(`/api/catalogs/${m.catalogId}/materials/${m.id}`);
  }

  // uploading texture changes texture, size, and sizey properties
  public uploadTexture(
    m: CatalogMaterial,
    file: File
  ): Observable<CatalogMaterial> {
    if (!m.id) {
      return this.addMaterial(m.catalogId, m.name).pipe(concatMap(mat =>
        this.uploadTexture(mat, file)
      ));
    }
    let data = new FormData();
    data.append('file', file, file.name);
    return this.http.post<CatalogMaterial>(
      `/api/catalogs/${m.catalogId}/materials/${m.id}/texture`,
      data
    );
  }

  public uploadBumpMap(
    m: CatalogMaterial,
    file?: File,
    bump = false
  ): Observable<CatalogMaterial> {
    let data = new FormData();
    if (file) {
      data.append('file', file, file.name);
    }
    return this.http.post<CatalogMaterial>(
      `/api/catalogs/${m.catalogId}/materials/${m.id}/bumptexture?bump=${bump}`,
      data
    );
  }

  public getProperties(catalogId: number): Observable<CatalogProperty[]> {
    return this.http.get<CatalogProperty[]>(
      `/api/catalogs/${catalogId}/properties`
    );
  }

  public getProperty(
    catalogId: number,
    propertyId: number
  ): Observable<CatalogProperty> {
    return this.http.get<CatalogProperty>(
      `/api/catalogs/${catalogId}/properties/${propertyId}`
    );
  }

  public addProperty(
    catalogId: number,
    name: string,
    data?: string
  ): Observable<CatalogProperty> {
    return this.http.post<
      CatalogProperty
    >(`/api/catalogs/${catalogId}/properties`, { name, data });
  }

  public setProperty(
    property: CatalogProperty
  ): Observable<CatalogProperty> {
    return this.http.post<CatalogProperty>(
      `/api/catalogs/${property.catalogId}/properties/${property.id}`,
      property
    );
  }

  public removeProperty(catalogId: number, propertyId: number) {
    return this.http.delete(
      `/api/catalogs/${catalogId}/properties/${propertyId}`
    );
  }
}





