import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  checkSendEmail = 0;
  email = "";
  authInProgress = false;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  login(email: string) {
    this.auth.requestEmailToken(email).subscribe(
      _ => { this.checkSendEmail = 1; this.authInProgress = false },
      _ => { this.checkSendEmail = -1; this.authInProgress = false }
    );
  }

  goToFiles(email: string, token: string) {
    this.auth.emailLogin(email, token).
      subscribe(_ => { this.router.navigate(['/files']); this.authInProgress = false },
        _ => { this.checkSendEmail = -2; this.authInProgress = false });
  }
}
