import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';

export interface FileItem {
  id: number;
  name: string;
  preview: string;
  folder: boolean;
  shared?: boolean;
  readOnly?: boolean;
  // returned by GetFile() only
  parentId?: number;
  catalogId?: number;
  price?: number;
  sku: string;
  modifiedAt: string;
  ownerName?: string;
  files?: FileItem[];
}

export interface MoveFilesResult {
  error?: 'permission' | 'recursion',
  folder: FileItem,
  materials: string[];
}

function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  let byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
  // separate out the mime component
  let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  // write the bytes of the string to a typed array
  let ia = new Uint8Array(byteString.length);
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ia], { type: mimeString });
}

export interface FilesUploadResponse {
  uploaded: any[];
  failed: any[];
  newMaterials: any[];
}

@Injectable({
  providedIn: 'root',
})
export class FilesService {
  constructor(
    private http: HttpClient
  ) {}

  public getFile(id: number, children = false): Observable<FileItem> {
    let url = `/api/files/${id}`;
    if (children) {
      url += '?children=true';
    }
    return this.http.get<FileItem>(url).pipe(map(f => {
      if (f.files) {
        f.files.sort(this.fileSort);
      }
      return f;
    }));
  }

  public getFiles(ids: number[], strict = false): Observable<FileItem[]> {
    let url = `/api/files/items/${ids.join('+')}`;
    if (strict) {
      url += '?strict=true';
    }
    return this.http.get<FileItem[]>(url);
  }

  private fileSort = (f1: FileItem, f2: FileItem) => {
    let result = Number(f2.folder) - Number(f1.folder);
    if (result === 0) {
      result = f1.name.localeCompare(f2.name);
    }
    return result;
  }

  public getChildren(folder: number): Observable<FileItem[]> {
    return this.http
      .get<FileItem[]>(`/api/files/${folder}/items/`)
      .pipe(map(items => items.sort(this.fileSort)));
  }

  getSharedProjects(): Observable<FileItem[]> {
    return this.http.get<FileItem[]>(`/api/files/projects`).pipe(map(files => {
      files.forEach(f => f.name = f.name || '');
      return files;
    }));
  }

  backupProject(projectId: number, name?: string) {
    let params = name ? {name} : undefined;
    return this.http.post<File>(`/api/files/${projectId}/backup`, {}, {params});
  }

  restoreProject(projectId: number, backupId: string | number) {
    return this.http.post<boolean>(`/api/files/${projectId}/restore/${backupId}`, {});
  }

  getProjectBackups(projectId: number): Observable<FileItem[]> {
    return this.http.get<FileItem[]>(`/api/files/${projectId}/backups`);
  }

  public removeFile(file: FileItem) {
    return this.http.delete(`/api/files/${file.id}`);
  }

  public removeFiles(files: FileItem[]) {
    return this.http.post(`/api/files/delete`, { files: files.map(f => f.id)});
  }

  public restoreFiles(files: FileItem[]) {
    return this.http.post(`/api/files/restore`, { files: files.map(f => f.id)});
  }

  public renameFile(file: FileItem, newName: string) {
    return this.http.post<FileItem>(`/api/files/${file.id}`, { name: newName });
  }

  public setPrice(file: FileItem, price: number) {
    return this.http.post(`/api/files/${file.id}`, { price });
  }

  public setSku(file: FileItem, sku: string) {
    return this.http.post(`/api/files/${file.id}`, { sku });
  }

  public share(file: FileItem, shared: boolean) {
    return this.http.post(`/api/files/${file.id}`, { shared });
  }

  public addFolder(parentId: number, name: string) {
     return this.http.post<FileItem>(`/api/files`, {name, parentId, type: "Folder"});
  }

  public moveFiles(source: number[], destination: number) {
    return this.http.post<MoveFilesResult>(`/api/files/move`, {source, destination});
  }

  public uploadFile(folderOrFileId: number, file: File, extractPreview = true) {
    let data = new FormData();
    data.append('file', file, file.name);
    let url = `./api/files/${folderOrFileId}/upload?preview=${extractPreview}`;
    return this.http.post<FilesUploadResponse>(url, data)
  }

  public updateThumbnail(fileId: string, thumbnailData: string) {
    let data = new FormData();
    let thumbBlob = dataURItoBlob(thumbnailData);
    data.append('file', thumbBlob, 'thumb.png')
    return this.http.post<{ preview: string}>(`/api/files/${fileId}/preview`, data);
  }

  public updateCustomThumbnail(fileId: number, thumbnail: File) {
    let data = new FormData();
    data.append('file', thumbnail, thumbnail.name);
    return this.http.post<{ preview: string}>(`/api/files/${fileId}/custompreview`, data);
  }

  public removeThumbnail(id: number) {
    return this.http.delete(`/api/files/${id}/preview`);
  }
}
