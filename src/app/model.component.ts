import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilesService, FileItem } from './files.service';
import { concatMap } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {

  file$: Observable<FileItem>;

  constructor(route: ActivatedRoute, file: FilesService) {
    this.file$ = route.params.pipe(concatMap(p => file.getFile(Number(p['id']))))
   }

  ngOnInit() {
  }

}
