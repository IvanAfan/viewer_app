import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowseComponent } from './browse.component';
import { FilesComponent } from './files.component';
import { LoginComponent } from './login.component';
import { AppRoutingModule } from './/app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatTableModule,
   MatSelectModule, MatProgressSpinnerModule, MatFormFieldModule,
    MatInputModule, MatListModule, MatIconModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatTooltipModule} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthComponent } from './auth.component';
import { AuthInterceptor } from './auth.service';
import { ModelComponent } from './model.component';
import { DelDialogComponent } from './del-dialog/del-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    BrowseComponent,
    FilesComponent,
    LoginComponent,
    AuthComponent,
    ModelComponent,
    DelDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, BrowserAnimationsModule, MatButtonModule, MatCheckboxModule, MatProgressSpinnerModule,
    MatFormFieldModule, MatTableModule, MatSelectModule, MatListModule, MatIconModule, FormsModule, HttpClientModule, FlexLayoutModule,
     MatInputModule, MatDialogModule, MatTooltipModule
    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    // {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent],
  entryComponents: [DelDialogComponent]
})
export class AppModule { }

